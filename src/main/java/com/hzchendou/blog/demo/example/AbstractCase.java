package com.hzchendou.blog.demo.example;

import java.io.IOException;
import java.io.InputStream;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

/**
 * @Date: 2022-07-04 15:47
 * @since: 1.0
 */
@Slf4j
public abstract class AbstractCase {
    protected static SqlSessionFactory sqlSessionFactory = null;
    static {
        /// 读取配置文件，初始化mybatis
        String resource = "mybatis.xml";
        InputStream inputStream = null;
        try {
            inputStream = Resources.getResourceAsStream(resource);
            /// 创建SqlSession工厂对象，方便后续创建SqlSession，使用SqlSession操作数据库
            sqlSessionFactory = new SqlSessionFactoryBuilder().build(inputStream);
        } catch (IOException e) {
            log.error("创建Mybatis SqlSessionFactory 失败", e);
        }

    }
    public abstract void run() throws IOException;
}
