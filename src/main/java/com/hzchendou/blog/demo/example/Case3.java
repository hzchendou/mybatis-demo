package com.hzchendou.blog.demo.example;

import com.hzchendou.blog.demo.entity.BlogDO;
import com.hzchendou.blog.demo.mapper.BlogMapper;
import java.io.IOException;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.ibatis.session.SqlSession;

/**
 * 案例三- 缓存
 *
 * @Date: 2022-07-07 18:11
 * @since: 1.0
 */
@Slf4j
public class Case3 extends AbstractCase {

    @Override
    public void run() throws IOException {
        /// 一级缓存
//        cacheWithLevel1();
        /// 二级缓存
        cacheWithLevel2();
    }

    /**
     * 使用一级缓存案例
     */
    private void cacheWithLevel1() {
        /// 使用Java 7提供的 try-with-resource写法，让JVM自动完成资源关闭操作
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //// 获取Mapper接口示例（底层使用了JDK的Proxy.newProxyInstance方法创建代理）
            BlogMapper blogMapper = sqlSession.getMapper(BlogMapper.class);
            /// 直接查询
            List<BlogDO> blogs = blogMapper.selectAll();
            log.info("查询博客文章列表信息, {}", blogs);
            blogs = blogMapper.selectAll();
            log.info("查询博客文章列表信息, {}", blogs);
        }
    }

    /**
     * 使用二级缓存案例
     */
    private void cacheWithLevel2() {
        /// 使用Java 7提供的 try-with-resource写法，让JVM自动完成资源关闭操作
        try (SqlSession sqlSession = sqlSessionFactory.openSession()) {
            //// 获取Mapper接口示例（底层使用了JDK的Proxy.newProxyInstance方法创建代理）
            BlogMapper blogMapper = sqlSession.getMapper(BlogMapper.class);
            /// 直接查询
            List<BlogDO> blogs = blogMapper.selectAll();
            log.info("查询博客文章列表信息, {}", blogs);
            sqlSession.commit();
            blogs = blogMapper.selectAll();
            log.info("查询博客文章列表信息, {}", blogs);
        }
    }
}
