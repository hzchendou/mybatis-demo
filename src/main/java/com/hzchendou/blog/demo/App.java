package com.hzchendou.blog.demo;

import com.hzchendou.blog.demo.example.Case4;
import java.io.IOException;
import lombok.extern.slf4j.Slf4j;

/**
 * Hello world!
 */
@Slf4j
public class App {

    public static void main(String[] args) throws IOException {
        /// 案例1，
        // 实现创建记录、查询记录、打印日志、提交事务、关闭连接
//        Case1 case1 = new Case1();
//        case1.run();

        /// 案例2，
        // 使用TypeHandler 实现类型转换
//        Case2 caseInfo = new Case2();
//        caseInfo.run();

        /// 案例3，
        // 使用 缓存
//        Case3 caseInfo = new Case3();
//        caseInfo.run();


        //// 分页查询
        Case4 case4 = new Case4();
        case4.run();
    }
}
