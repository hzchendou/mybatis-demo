package com.hzchendou.blog.demo;

import com.hzchendou.blog.demo.entity.BlogDO;
import com.hzchendou.blog.demo.service.BlogService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

/**
 * @Date: 2022-07-09 22:08
 * @since: 1.0
 */
@Slf4j
@MapperScan(basePackages = "com.hzchendou.blog.demo.mapper")
@SpringBootApplication
public class SpringBootApp {

    public static void main(String[] args) {
        ApplicationContext context = SpringApplication.run(SpringBootApp.class);
//        BlogMapper blogMapper = context.getBean(BlogMapper.class);
//        List<BlogDO> blogs = blogMapper.selectAll();
//        log.info("查询博文记录, {}", blogs);
        BlogService blogService = context.getBean(BlogService.class);
        List<BlogDO> blogs = blogService.selectAll();
        log.info("查询博文记录, {}", blogs);
        blogs = blogService.selectAll();

        List<BlogDO> pageBlogs = blogService.selectPage();
        log.info("查询博文分页记录, {}", pageBlogs);
    }
}
