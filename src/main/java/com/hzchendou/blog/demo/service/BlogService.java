package com.hzchendou.blog.demo.service;

import com.hzchendou.blog.demo.entity.BlogDO;
import com.hzchendou.blog.demo.mapper.BlogMapper;
import com.hzchendou.blog.demo.params.PageVO;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 博文服务
 *
 * @Date: 2022-07-11 10:39
 * @since: 1.0
 */
@Service
public class BlogService {

    @Autowired
    private BlogMapper blogMapper;

    @Transactional
    public List<BlogDO> selectAll() {
        return blogMapper.selectAll();
    }

    public List<BlogDO> selectPage() {
        PageVO<BlogDO> pageVO = selectPage(1,2);
        return pageVO.getRecords();
    }

    private PageVO<BlogDO> selectPage(int page, int size) {
        PageVO<BlogDO> param = new PageVO();
        param.setPage(page);
        param.setSize(size);
        List<BlogDO> blogs = blogMapper.selectPage(param);
        param.setRecords(blogs);
        param.setPageSize(blogs == null ? 0 : blogs.size());
        return param;
    }

}
