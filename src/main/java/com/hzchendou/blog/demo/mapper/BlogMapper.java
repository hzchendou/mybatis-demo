package com.hzchendou.blog.demo.mapper;

import com.hzchendou.blog.demo.entity.BlogDO;
import com.hzchendou.blog.demo.params.PageVO;
import java.util.List;

/**
 * 博文信息Mapper
 *
 * @Date: 2022-07-04 14:25
 * @since: 1.0
 */
public interface BlogMapper {

    /**
     * 查询所有记录
     *
     * @return
     */
    List<BlogDO> selectAll();

    /**
     * 添加记录
     *
     * @param blogDO
     * @return
     */
    int insert(BlogDO blogDO);

    /**
     * 分页查询
     *
     * @param param
     * @return
     */
    List<BlogDO> selectPage(PageVO param);
}
