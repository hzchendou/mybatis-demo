package com.hzchendou.blog.demo.mapper;

import com.hzchendou.blog.demo.entity.AuthorDO;
import java.util.List;

/**
 * 作者信息Mapper
 *
 * @Date: 2022-07-04 14:47
 * @since: 1.0
 */
public interface AuthorMapper {

    /**
     * 查询所有记录
     *
     * @return
     */
    List<AuthorDO> selectAll();

    /**
     * 添加记录
     *
     * @param authorDO
     * @return
     */
    int insert(AuthorDO authorDO);
}
