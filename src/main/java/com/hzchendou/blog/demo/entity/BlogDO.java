package com.hzchendou.blog.demo.entity;

import com.hzchendou.blog.demo.enums.BlogStatusEnums;
import java.io.Serializable;
import lombok.Data;

/**
 * 博文DO
 *
 * @Date: 2022-07-04 14:49
 * @since: 1.0
 */
@Data
public class BlogDO implements Serializable {

    /**
     * 文章ID
     */
    private Integer id;
    /**
     * 文章标题
     */
    private String title;
    /**
     * 文章作者Id
     */
    private Integer authorId;
    /**
     * 标签列表
     */
    private String tags;
    /**
     * 文章状态
     */
    private BlogStatusEnums status;
}
