package com.hzchendou.blog.demo.entity;

import lombok.Data;

/**
 * 作者DO
 *
 * @Date: 2022-07-04 14:49
 * @since: 1.0
 */
@Data
public class AuthorDO {
    private Integer id;
    private String username;
    private String remark;
}
