package com.hzchendou.blog.demo.params;

import java.util.List;
import lombok.Data;

/**
 * @Date: 2022-07-08 15:30
 * @since: 1.0
 */
@Data
public class PageVO<T> {
    private int page;
    private int size;
    private Long total;
    private Integer pageSize;
    private List<T> records;
}
