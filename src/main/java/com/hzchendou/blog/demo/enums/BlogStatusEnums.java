package com.hzchendou.blog.demo.enums;

/**
 * 博客状态枚举类型
 *
 * @Date: 2022-07-07 10:25
 * @since: 1.0
 */
public enum BlogStatusEnums {
    INVALID,
    VALID
}
