CREATE TABLE IF NOT EXISTS author(
    id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
    username varchar(30) NOT NULL,
    remark varchar (1000) NULL,
    PRIMARY KEY(id)
);

CREATE TABLE "blog" (
  "id" INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
  "title" varchar(30) NOT NULL,
  "author_id" int(11) NOT NULL,
  "tags" varchar(200) NOT NULL,
  "status" int(10) NOT NULL
);